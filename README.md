# EAP-IUMR-IdLR_HealthinRescue

This project is to develop an IoT device, that is an ‘Emergency Access Provisioned, Integrated Unique Medical Record Id Local Registry’ (EAP-IUMR-IdLR), along with an App called ‘HealthinRescue’. 
In Normal conditions, this EAP-IUMR-IdLR is being operated in coordination with another App called ‘HealthnetEntry’, that is the base product of an Asynchronous Realtime Cyber-physical Healthcare system called, ‘Healthnet System’. While using this App to access the Medical Records of an individual, the Integrated Unique Medical Record Id (IUMR-Id) and the addresses of the Healthcare-IoT devices & wearables of that individual are stored in this device.
In Emergency conditions, this device and some of the use cases of HealthinRescue App are authenticated to access these data for the recently visited individuals to access their Medical Records, by the Local and Regional Governmental Health Authorities. This authentication to be permitted by United Nations Disaster Management Regulatory Authority, under Human right legal provisions.

**Abstract of this project is described in:**

Description.mp3

**Details of this device and App are described in:**

EAP-IUMR-IdLR_IoT_device_HealthinRescue_ExtentedDetails.pdf

**Audio accessible file:**

EAP-IUMR-IdLR_IoT_device_HealthinRescue_Audio.wma

**Roadmap for this project is in:**

EAP-IUMR-IdLR_IoT_w_DualApp_Roadmap_Description.pdf
